import React from 'react'
import SignInForm from './sign-in'
import EditTasks from './edit-tasks'
import {logIn, checkAuthStatus} from '../../AC/auth'
import {connect} from "react-redux";
import {CircularProgress} from "@material-ui/core";
import {Link} from "react-router-dom";

const mapStateToProps = (state, ownProps) => {
    return {
        isAuthorized: state.auth.isAuthorized,
        loading: state.auth.loading,
        error: state.auth.error,
    }
};

const mapDispatchToProps = dispatch => (
    {
        logIn: (login, password) => {
            dispatch(logIn(login, password))
        },
        checkAuthStatus: () => {
            dispatch(checkAuthStatus())
        }
    }
);

class Admin extends React.Component{
    constructor() {
        super();
    }

    componentWillMount() {
        this.props.checkAuthStatus();
    }

    render(){
        let me = this;

        let handleSubmit = (data) => {
          me.props.logIn(data.username, data.password);
        };

        if (this.props.isAuthorized) {
            return <EditTasks/>;
        } else {
            if (this.props.loading) {
                return <CircularProgress size={32} style={{marginTop: 15}}/>
            } else {
                return (
                    <div>
                        <div><Link to={'/'} style={{marginBottom: 15}}> Home </Link></div>

                        <SignInForm onSubmit={handleSubmit} />
                        {this.props.error && <div style={{marginTop: 15}}>ERROR: {this.props.error} </div>}
                    </div>
                )

            }
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Admin);