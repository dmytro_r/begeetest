import React from 'react'
import { Field, reduxForm } from 'redux-form'
import TextField from '@material-ui/core/TextField/index'
import Button from '@material-ui/core/Button/index';

const validate = values => {
    const errors = {};
    if (!values.username) {
        errors.username = 'Required'
    }

    if (!values.password) {
        errors.password = 'Required'
    }

    return errors
};

const renderTextField = ({
                             label,
                             input,
                             meta: { touched, invalid, error },
                             ...custom
                         }) => (
    <TextField
        label={label}
        placeholder={label}
        error={touched && invalid}
        helperText={touched && error}
        {...input}
        {...custom}
    />
);

const SignInForm = (props) => {
    const { handleSubmit, pristine, submitting } = props;
    return (
        <form style={{marginTop: '15px'}} onSubmit={handleSubmit}>
            <div><Field name="username" type="text" component={renderTextField} label="Username"/></div>
            <div><Field name="password" type="text" component={renderTextField} label="Password"/></div>
            <div>
                <Button style={{marginTop: '15px', marginBottom: '15px'}} variant="contained" color="primary" type="submit" disabled={submitting}>Login</Button>
            </div>
        </form>
    )
};

export default reduxForm({
    form: 'login',
    validate,
})(SignInForm)