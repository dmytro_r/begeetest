import React from 'react'
import Button from "@material-ui/core/Button";
import {connect} from "react-redux";
import {logOut} from "../../AC/auth";
import {Link} from "react-router-dom";
import {getTasks, editTask} from '../../AC/edit'
import
    "@devexpress/dx-react-grid";
import EditableTable from './editableTable'

const mapStateToProps = (state, ownProps) => {
    return {
        isAuthorized: state.auth.isAuthorized,

        tasksApiData: state.edit.tasks,
        loading: state.edit.loading,
        error: state.edit.error,
        sortField: state.edit.sortField,
        sortDirection: state.edit.sortDirection,
        page: state.edit.page,

    }
};

const mapDispatchToProps = dispatch => (
    {
        logOut: () => {
            dispatch(logOut())
        },
        getTasks: (sortField, sortDirection, page) => {
            dispatch(getTasks(sortField, sortDirection, page))
        },
        editTask: (id, text, status) => {
            dispatch(editTask(id, text, status))
        },
    }
);

class EditTasks extends React.Component{
    constructor() {
        super();
        this.state = {editingRowIds: [], rowChanges: {}}
    }

    componentWillMount() {
        this.props.getTasks(null, null, 0);
    }

    render(){

        let me = this;
        let handleLogout = ()=>{
            me.props.logOut();
        };
        const data = this.props.tasksApiData ? this.props.tasksApiData.tasks : [];
        let totalCount = data.length ? parseInt(this.props.tasksApiData.total_task_count) : 0;

        let sorting = {};
        me.props.sortField && (sorting['columnName'] = me.props.sortField);
        me.props.sortDirection && (sorting['direction'] = me.props.sortDirection);

        const commitChanges = ({ changed }) => {
            if (changed) {
                let updatedRowIndex = parseInt(Object.keys(changed).pop());
                let dataRow = data.find(row => row.id === updatedRowIndex);
                let updatedRow = {...dataRow, ...changed[updatedRowIndex]};
                me.props.editTask(updatedRow.id, updatedRow.text, updatedRow.status)
            }
        };

        return (
            <div>
                <div><Link to={'/'} style={{margin: 16, display:'inline-block'}}> Home </Link> <Button style={{marginTop: '17px', marginBottom: '15px'}}
                                                                                                variant="contained"
                                                                                                color="primary"
                                                                                                onClick={handleLogout}>Logout</Button></div>
                <EditableTable
                    data={data}
                    sorting={sorting}
                    onSortingChange={sorting => {me.props.getTasks(sorting[0].columnName, sorting[0].direction, me.props.page)}}
                    totalCount={totalCount}
                    defaultCurrentPage={me.props.page}
                    onCurrentPageChange={page => me.props.getTasks(me.props.sortField, me.props.sortDirection, page)}
                    commitChanges={commitChanges}
                    loading={me.props.loading}
                />


                {me.props.error && <div>ERROR: {me.props.error}</div>}
            </div>
        );

    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EditTasks);