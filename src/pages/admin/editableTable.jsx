import React, { useState } from 'react';
import {
    SortingState, EditingState, PagingState, CustomPaging,
} from '@devexpress/dx-react-grid';
import {
  Grid,
  Table, TableHeaderRow, TableEditRow, TableEditColumn,
  PagingPanel,
} from '@devexpress/dx-react-grid-material-ui';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import SaveIcon from '@material-ui/icons/Save';
import CancelIcon from '@material-ui/icons/Cancel';
import Checkbox from "@material-ui/core/Checkbox";
import TableCell from "@material-ui/core/TableCell";
import {CircularProgress} from "@material-ui/core";

const EditButton = ({ onExecute }) => (
    <IconButton onClick={onExecute} title="Edit row">
      <EditIcon />
    </IconButton>
);

const CommitButton = ({ onExecute }) => (
    <IconButton onClick={onExecute} title="Save changes">
      <SaveIcon />
    </IconButton>
);

const CancelButton = ({ onExecute }) => (
    <IconButton color="secondary" onClick={onExecute} title="Cancel changes">
      <CancelIcon />
    </IconButton>
);

const commandComponents = {
  edit: EditButton,
  commit: CommitButton,
  cancel: CancelButton,
};

const Command = ({ id, onExecute }) => {
  const CommandButton = commandComponents[id];
  return (
      <CommandButton
          onExecute={onExecute}
      />
  );
};

const Cell = (props) => {
    const { column, value  } = props;

    if (column.name === 'status') {
        return <Table.Cell><Checkbox
            checked={value === 10}
            inputProps={{
                'aria-label': 'primary checkbox',
            }}
        /></Table.Cell>
    }

    return <Table.Cell {...props} />;
};

const EditCell = (props, ...rest) => {
    const { column, value } = props;

    if (column.name === 'email' || column.name === 'username') {
        return <TableCell>{value}</TableCell>;
    }

    if (column.name === 'status') {
        return  <Table.Cell><Checkbox
            checked={value === 10}
            onChange={(e, val)=>props.onValueChange(val ? 10 : 0)}
            value="status"
            inputProps={{
                'aria-label': 'primary checkbox',
            }}
        /></Table.Cell>
    }

    return <TableEditRow.Cell {...props} />;
};

const getRowId = row => row.id;

export default (props) => {
    let columns = [
        {name: "email", title: "email"},
        {name: "username", title: "username"},
        {name: "status", title: "status"},
        {name: "text", title: "text"}
    ];
  const [rows, setRows] = useState([]);
  const [editingRowIds, getEditingRowIds] = useState([]);
  const [rowChanges, setRowChanges] = useState({});

    let onEditingRowIdsChange = (editingRowIds)=> {
        let editingRows = [editingRowIds.pop()];
        getEditingRowIds(editingRows);
        return editingRows;
    };

  return (
      <Paper>
          <Grid
              rows={props.data}
              columns={columns}
              getRowId={getRowId}
          >
              <EditingState
                  editingRowIds={[...editingRowIds]}
                  onEditingRowIdsChange={onEditingRowIdsChange}
                  rowChanges={rowChanges}
                  onRowChangesChange={setRowChanges}
                  addedRows={[]}
                  onAddedRowsChange={() => {
                  }}
                  onCommitChanges={props.commitChanges}
              />

              <Table
                  cellComponent={Cell}
              />
              <SortingState
                  sorting={[props.sorting]}
                  onSortingChange={props.onSortingChange}
              />
              <PagingState
                  defaultCurrentPage={props.page}
                  pageSize={3}
                  onCurrentPageChange={props.onCurrentPageChange}
              />
              <CustomPaging
                  totalCount={props.totalCount}
              />
              <TableHeaderRow showSortingControls/>
              <TableEditRow
                  cellComponent={EditCell}
              />
              <TableEditColumn
                  width={170}
                  showEditCommand
                  commandComponent={Command}
              />
              <PagingPanel />
          </Grid>
          {props.loading && <CircularProgress style={{marginBottom: 15}} size={24}/>}
      </Paper>
  );
};
