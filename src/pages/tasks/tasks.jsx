import React from 'react'
import {connect} from 'react-redux'
import {getTasks, addTask} from '../../AC/tasks'
import {CircularProgress, Typography} from '@material-ui/core';
import MUIDataTable from "mui-datatables";
import SyncValidationForm from './addTaskForm'
import {Link} from 'react-router-dom'
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';

const mapStateToProps = (state, ownProps) => {
    return {
        tasksApiData: state.tasks,
        loading: state.tasks.loading,
        error: state.tasks.error,
        lastAddedTask: state.tasks.lastAddedTask,
        sortField: state.tasks.sortField,
        sortDirection: state.tasks.sortDirection,
        page: state.tasks.page
    }
};

const mapDispatchToProps = dispatch => (
    {
        getTasks: (sortField, sortDirection, page) => {
            dispatch(getTasks(sortField, sortDirection, page))
        },
        addTask: (username, email, text) => {
            dispatch(addTask(username, email, text))
        },
    }
);

class Tasks extends React.Component {
    constructor() {
        super();
        this.state = {
            sortField: void 0,
            sortDirection: void 0
        }
    }

    componentWillMount() {
        this.props.getTasks();
    }

    render() {
        let me = this;
        const options = {
            filterType: 'none',
            print: false,
            download: false,
            selectableRows: 'none',
            rowsPerPage: 3,
            rowsPerPageOptions: [3],
            count: 0,
            pagination: true,
            serverSide: true,
            onTableChange: (action, tableState) => {
                let currSortField = tableState.activeColumn !== null ? tableState.columns[tableState.activeColumn].name : null;
                let sortDirection = currSortField ? tableState.columns[tableState.activeColumn ].sortDirection : null;
                let page = tableState.page;
                switch (action) {
                    case 'changePage':
                        this.props.getTasks(currSortField, sortDirection, page);
                        break;

                    case 'sort':
                        this.props.getTasks(currSortField, sortDirection, page);
                        break;
                }
            },
            customRowRender: (data, dataIndex, rowIndex) => {
                const [name, email, text, status ] = data;
                let tdStyle = { padding: "10px", textAlign: 'left'};
                let statusIcon = status === 10 ? <CheckBoxIcon/> : <CheckBoxOutlineBlankIcon/>
                return (
                    <tr key={rowIndex} style={{ paddingTop: "30px"}} >
                        <td  style={tdStyle}>
                            <span>{name}</span>
                        </td>
                        <td style={tdStyle}>
                            {email}
                        </td>
                        <td style={tdStyle}>
                            <pre>{text}</pre>
                        </td>
                        <td style={tdStyle}>
                            {statusIcon}
                        </td>
                      </tr>
                );
            }

        };

        const columns = ["name", "email", "text", "status"];

        for (let i = 0; i < columns.length; i++) {
            let currCol = columns[i];
            if(currCol === me.props.sortField) {
                columns[i] = {
                    name: currCol,
                    options: {
                        sortDirection:me.props.sortDirection
                    }
                }
            }
        }

        let data = ( this.props.tasksApiData && this.props.tasksApiData.tasks ) ? this.props.tasksApiData.tasks.tasks.map(row => [row.username, row.email, row.text, row.status]) : [];
        data.length && ( options.count = parseInt(this.props.tasksApiData.tasks.total_task_count ) );

        let handleAddTaskSubmit = (formVals) => {
            me.props.addTask(formVals.username, formVals.email, formVals.text);
        };

        return (
            <div>
                <MUIDataTable title={<Typography variant="inherit">
                    Tasks List
                    {this.props.loading &&
                    <CircularProgress size={24} style={{marginLeft: 15, position: 'relative', top: 4}}/>}
                </Typography>
                } data={data} columns={columns} options={options}/>

                <SyncValidationForm onSubmit={handleAddTaskSubmit} />


                {me.props.error && <div>ERROR: {me.props.error}</div>}
                {me.props.lastAddedTask && <div>SUCCESS: {me.props.lastAddedTask}</div>}

                <Link to={'/admin'} style={{marginTop: 15}}> Admin </Link>
            </div>
        )
    }
}


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Tasks);
