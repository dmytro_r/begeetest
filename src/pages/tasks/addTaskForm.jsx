import React from 'react'
import { Field, reduxForm } from 'redux-form'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button';

const validate = values => {
    const errors = {};
    if (!values.username) {
        errors.username = 'Required'
    } else if (values.username.length > 15) {
        errors.username = 'Must be 15 characters or less'
    }
    if (!values.email) {
        errors.email = 'Required'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = 'Invalid email address'
    }
    if (!values.text) {
        errors.text = 'Required'
    } else if (values.username.length > 15) {
        errors.text = 'Must be 15 characters or less'
    }
    return errors
};

const renderField = ({ input, label, type, meta: { touched, error, warning } }) => (
    <div>
        <label>{label}</label>
        <div>
            <input {...input} placeholder={label} type={type}/>
            {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    </div>
);

const renderTextField = ({
                             label,
                             input,
                             meta: { touched, invalid, error },
                             ...custom
                         }) => (
    <TextField
        label={label}
        placeholder={label}
        error={touched && invalid}
        helperText={touched && error}
        {...input}
        {...custom}
    />
);
const SyncValidationForm = (props) => {
    const { handleSubmit, pristine, submitting } = props;
    return (
        <form style={{marginTop: '15px'}} onSubmit={handleSubmit}>
            <div><Field name="username" type="text" component={renderTextField} label="Username"/></div>
            <div><Field name="email" type="email" component={renderTextField} label="Email"/></div>
            <div><Field name="text" type="text" component={renderTextField} label="Text"/></div>
            <div>
                <Button style={{marginTop: '15px', marginBottom: '15px'}} variant="contained" color="primary" type="submit" disabled={submitting}> Submit</Button>
            </div>
        </form>
    )
};

export default reduxForm({
    form: 'taskAdd',
    validate,
})(SyncValidationForm)