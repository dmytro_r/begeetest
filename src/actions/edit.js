import {
    EDIT_FETCH_TASKS_IN_PROGRESS,
    EDIT_FETCH_TASKS_SUCCESS,
    EDIT_FETCH_TASKS_ERROR,

    EDIT_COMMIT_IN_PROGRESS,
    EDIT_COMMIT_SUCCESS,
    EDIT_COMMIT_ERROR
} from '../const'

export function tasksRequested(params) {
    return {
        type: EDIT_FETCH_TASKS_IN_PROGRESS,
        payload: params
    }
}

export function tasksFetchSuccess(payload) {
    return {
        type: EDIT_FETCH_TASKS_SUCCESS,
        payload: payload
    }
}

export function tasksFetchError(msg) {
    return {
        type: EDIT_FETCH_TASKS_ERROR,
        payload: msg
    }
}

export function commitRequested(params) {
    return {
        type: EDIT_COMMIT_IN_PROGRESS,
        payload: params
    }
}

export function commitSuccess(payload) {
    return {
        type: EDIT_COMMIT_SUCCESS,
        payload: payload
    }
}

export function commitError(msg) {
    return {
        type: EDIT_COMMIT_ERROR,
        payload: msg
    }
}