import {
    AUTH_IN_PROGRESS,
    AUTH_SUCCESS,
    AUTH_ERROR,
    AUTH_LOGOUT
} from '../const'

export function authRequested(params) {
    return {
        type: AUTH_IN_PROGRESS,
        payload: params
    }
}

export function authSuccess(payload) {
    return {
        type: AUTH_SUCCESS,
        payload: payload
    }
}

export function authError(msg) {
    return {
        type: AUTH_ERROR,
        payload: msg
    }
}

export function logOut() {
    return {
        type: AUTH_LOGOUT,
        payload: void 0
    }
}