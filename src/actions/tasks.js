import {
    HOME_FETCH_TASKS_IN_PROGRESS,
    HOME_FETCH_TASKS_SUCCESS,
    HOME_FETCH_TASKS_ERROR,

    ADD_TASK_IN_PROGRESS,
    ADD_TASK_SUCCESS,
    ADD_TASK_ERROR
} from '../const'

export function tasksRequested(params) {
    return {
        type: HOME_FETCH_TASKS_IN_PROGRESS,
        payload: params
    }
}

export function tasksFetchSuccess(payload) {
    return {
        type: HOME_FETCH_TASKS_SUCCESS,
        payload: payload
    }
}

export function tasksFetchError(msg) {
    return {
        type: HOME_FETCH_TASKS_ERROR,
        payload: msg
    }
}

export function addTaskRequested() {
    return {
        type: ADD_TASK_IN_PROGRESS,
        payload: void 0
    }
}

export function addTaskSuccess(payload) {
    return {
        type: ADD_TASK_SUCCESS,
        payload: payload
    }
}

export function addTaskError(payload) {
    return {
        type: ADD_TASK_ERROR,
        payload: payload
    }
}
