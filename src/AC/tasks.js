import {
    tasksRequested,
    tasksFetchSuccess,
    tasksFetchError,

    addTaskRequested,
    addTaskSuccess,
    addTaskError

} from "../actions/tasks";

import {getTasks as getTasksApiCall, addTask as addTaskApiCall} from '../services/tasks'

function getTasks (sortField, sortDirection, page) {
    return dispatch => {
        dispatch(tasksRequested({sortField:sortField, sortDirection: sortDirection, page:page}));
        return getTasksApiCall(sortField, sortDirection, page).then(
            (response) => {
                if(response.data.status === "ok"){
                    dispatch(tasksFetchSuccess(response.data.message))
                } else {
                    dispatch(tasksFetchError(JSON.stringify(response.data.message)))
                }
            }
        ).catch(
            (e) => {dispatch(addTaskError(JSON.stringify(e)))}
        );
    }
}

export {getTasks};


function addTask (username, email, text) {
    return (dispatch, getState) => {
        dispatch(addTaskRequested());
        return addTaskApiCall(username, email, text).then(
            (response) => {
                if(response.data.status === "ok") {
                    let tasksParams = getState().tasks;
                    dispatch(addTaskSuccess(JSON.stringify(response.data.message)));
                    dispatch(getTasks(
                        tasksParams.sortField,
                        tasksParams.sortDirection,
                        tasksParams.page
                    ));
                }else {
                    dispatch(addTaskError(JSON.stringify(response.data.message)))
                }
            }
        ).catch(
            (e) => {dispatch(addTaskError(JSON.stringify(e)))}
        );
    }
}

export {addTask};