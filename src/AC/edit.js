import {
    tasksFetchSuccess,
    tasksRequested,
    tasksFetchError,

    commitRequested,
    commitSuccess,
    commitError
} from "../actions/edit";
import {getTasks as getTasksApiCall, editTask as editTaskApiCall} from "../services/tasks";
import {getToken} from '../services/auth'

function getTasks (sortField, sortDirection, page) {
    return dispatch => {
        dispatch(tasksRequested({sortField:sortField, sortDirection: sortDirection, page:page}));
        return getTasksApiCall(sortField, sortDirection, page).then(
            (response) => {
                if(response.data.status === "ok"){
                    dispatch(tasksFetchSuccess(response.data.message))
                } else {
                    dispatch(tasksFetchError(JSON.stringify(response.data.message)))
                }
            }
        ).catch(
            (e) => {dispatch(tasksFetchError(JSON.stringify(e)))}
        );
    }
}

export {getTasks};

function editTask (id, text, status) {
    return dispatch => {
        dispatch(commitRequested());

        return editTaskApiCall(getToken(), id, text , status).then(
            (response) => {
                if(response.data.status === "ok"){
                    dispatch(commitSuccess({id:id, text: text, status:status}))
                } else {
                    dispatch(commitError(JSON.stringify(response.data.message)))
                }
            }
        ).catch(
            (e) => {dispatch(commitError(JSON.stringify(e)))}
        );
    }
}

export {editTask};