import {
    authRequested,
    authSuccess,
    authError,
    logOut as logOutAction
} from "../actions/auth";

import {getToken, logIn as logInApiCall, setToken} from '../services/auth'

export function logIn(login, password) {
    return dispatch => {
        dispatch(authRequested({login:login, password: password}));
        return logInApiCall(login, password).then(
            (response) => {
                if(response.data.status === "ok"){
                    setToken(response.data.message.token);
                    dispatch(authSuccess(response.data.message))
                } else {
                    dispatch(authError(JSON.stringify(response.data.message)))
                }
            }
        ).catch(
            (e) => {dispatch(authError(JSON.stringify(e)))}
        );
    }
}

export function logOut(login, password) {
    return dispatch => {
        setToken('');
        dispatch(logOutAction());
    }
}

export function checkAuthStatus() {
    return dispatch => {
        if(getToken()) {
            dispatch(authSuccess({token: getToken()}));
        }
    }
}