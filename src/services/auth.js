import axios from 'axios'
import {API_entry, developer} from '../const'

export function logIn(username, password) {
    const config = {
        headers: {
            'content-type': 'multipart/form-data'
        }
    };

    const formData = new FormData();
    formData.append("username", username);
    formData.append("password", password);

    return axios.post(API_entry + 'login?developer=' + developer , formData, config)
}

export function getToken() {
    return localStorage.getItem('token');
}

export function setToken(token) {
    localStorage.setItem('token', token);
}