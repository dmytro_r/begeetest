import axios from 'axios'
import {API_entry, developer} from '../const'


export function getTasks(sortField, sortDirection, page){
    let params = {developer: developer};
    sortField && ( params['sort_field'] = sortField );
    sortDirection && ( params['sort_direction'] = sortDirection );
    page && ( params['page'] = page+1 );

    return axios.get(API_entry, {
        params: params
    })
}

export function addTask(username, email, text ) {
    const config = {
        headers: {
            'content-type': 'multipart/form-data'
        }
    };

    const formData = new FormData();
    formData.append("username", username);
    formData.append("email", email);
    formData.append("text", text);

    return axios.post(API_entry + 'create?developer=' + developer, formData, config)
}

export function editTask(token, id, text , status) {
    const config = {
        headers: {
            'content-type': 'multipart/form-data'
        }
    };
    const formData = new FormData();
    formData.append("token", token);
    formData.append("text", text);
    formData.append("status", status);

    return axios.post(API_entry + 'edit/' + id +'/?developer=' + developer, formData, config)
}