import {
    EDIT_FETCH_TASKS_IN_PROGRESS,
    EDIT_FETCH_TASKS_SUCCESS,
    EDIT_FETCH_TASKS_ERROR,

    EDIT_COMMIT_IN_PROGRESS,
    EDIT_COMMIT_SUCCESS,
    EDIT_COMMIT_ERROR
} from '../const'

const initialState = {
    tasks: void 0,
    loading: false,
    error: void 0,
    sortField: void 0,
    sortDirection: void 0,
    page: 1
};

function editTaskReducer(state = initialState, action) {

    switch (action.type) {
        case EDIT_FETCH_TASKS_IN_PROGRESS:
            return { ...state, loading: true, sortField: action.payload.sortField, sortDirection: action.payload.sortDirection , page: action.payload.page };

        case EDIT_FETCH_TASKS_SUCCESS:
            return { ...state, tasks: action.payload, error: void 0, loading: false };

        case EDIT_FETCH_TASKS_ERROR:
            return { ...state, error: action.payload, loading: false, tasks: void 0 };

        case EDIT_COMMIT_IN_PROGRESS:
            return { ...state, loading: true};

        case EDIT_COMMIT_SUCCESS:
            let tasks = state.tasks.tasks.map( task => {
                if(task.id === action.payload.id) {
                    task = {...task, ...action.payload}
                }
                return task;
            } );

            return { ...state, loading: false, tasks: {...state.tasks, ...{tasks: tasks}} };

        case EDIT_COMMIT_ERROR:
            return { ...state, error: action.payload};


        default:
            return state;
    }
}

export default editTaskReducer;