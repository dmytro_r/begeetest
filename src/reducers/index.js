import {combineReducers} from 'redux';
import tasksReducer from './tasks'
import authReducer from './auth'
import editTaskReducer from './edit'
import { reducer as reduxFormReducer } from 'redux-form';

export const rootReducer = combineReducers(
    {
        tasks: tasksReducer,
        auth: authReducer,
        edit: editTaskReducer,
        form: reduxFormReducer,
    }
);