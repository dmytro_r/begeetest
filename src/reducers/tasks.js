import {
    HOME_FETCH_TASKS_IN_PROGRESS,
    HOME_FETCH_TASKS_SUCCESS,
    HOME_FETCH_TASKS_ERROR,

    ADD_TASK_IN_PROGRESS,
    ADD_TASK_SUCCESS,
    ADD_TASK_ERROR

} from '../const'

const initialState = {
    tasks: void 0,
    loading: false,
    error: void 0,
    lastAddedTask: void 0,
    addingTaskInProgress: false,
    sortField: void 0,
    sortDirection: void 0,
    page: void 0
};

function tasksReducer(state = initialState, action) {

    switch (action.type) {
        case HOME_FETCH_TASKS_IN_PROGRESS:
            return { ...state, loading: true, sortField: action.payload.sortField, sortDirection: action.payload.sortDirection , page: action.payload.page  };

        case HOME_FETCH_TASKS_SUCCESS:
            return { ...state, tasks: action.payload, error: void 0, loading: false };

        case HOME_FETCH_TASKS_ERROR:
            return { ...state, error: action.payload, loading: false, tasks: void 0 };

        case ADD_TASK_IN_PROGRESS:
            return { ...state, addingTaskInProgress: true };

        case ADD_TASK_SUCCESS:
            return { ...state, lastAddedTask: action.payload };

        case ADD_TASK_ERROR:
            return { ...state, error: action.payload };

        default:
            return state;
    }
}

export default tasksReducer;