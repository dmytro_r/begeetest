import {
    AUTH_IN_PROGRESS,
    AUTH_SUCCESS,
    AUTH_ERROR,
    AUTH_LOGOUT
} from '../const'

const initialState = {
    isAuthorized: void 0,
    loading: false,
    error: void 0
};

function authReducer(state = initialState, action) {

    switch (action.type) {
        case AUTH_IN_PROGRESS:
            return { ...state, loading:true };

        case AUTH_SUCCESS:
            return { ...state, isAuthorized:true, loading: false };

        case AUTH_ERROR:
            return { ...state, error: action.payload, loading: false, isAuthorized: false };

        case AUTH_LOGOUT:
            return initialState;

        default:
            return state;
    }
}

export default authReducer;