import React from 'react';
import './App.css';
import history from "./history";
import { Router, Route } from  "react-router-dom";
import Tasks from './pages/tasks/tasks'
import Admin from './pages/admin/admin'

function App() {
    return (
        <div className="App">
            <Router history={history}>
                <Route exact path="/" component={ Tasks } />
                <Route exact path="/admin" component={ Admin } />
            </Router>
        </div>
    );
}

export default App;
